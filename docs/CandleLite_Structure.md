---
id: candlelite_structure
title: CandleLite Structure
sidebar_label: Structure
---

## At a Glance

CandleLite is a modular based framework that allows for different tools
to be injected into the framework and run as a single system.  To accomplish
this, the framework was built from the ground up to be modular.  From new
modules to the core itself, every component is replaceable.

How does this flexibility help us?  Modularity allows developers to build
modules without the need to run the entire framework.  Another benefit of
modularity is that it allows ToolAssist to try out new modules easily.

## What is a Module

Modules are self-contained applications.  They contain all of the code
required for the module to run and make decisions.  There are a few
exceptions to this that are provided by the framework, but do not have
to be used.  The module must conform to specific structures for the
framework to acknowledge it.  We will discuss these in more detail.

## Why Modules?

Modules were decided for flexibility.  We recognize that everyone has
different preferences when it comes to languages and frameworks.  As such,
we have developed the module framework to access a variety of front-end
frameworks.  We want the system to as flexible as possable while still
feeling as if it is a single unit.

## What can i write it in?

Currently, the Module Framework only supports a Typescript/javascript
back-end.  The Module Framework does support any front-end framework by
expecting the output of the module to be compiled front-end pages.

Later plans include supporting a WASM style of modules which would
allow the developer to use any language for any part of the framework.

 
